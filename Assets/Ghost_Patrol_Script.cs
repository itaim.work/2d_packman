using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost_Patrol_Script : Ghost_Script
{

    [SerializeField] private GameObject[] WayPoints;
    public int WayPointCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        BasePosition = transform.position;
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
        WayPoints = GameObject.FindGameObjectsWithTag("WayPoint");
        agent.SetDestination(WayPoints[WayPointCounter++].transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, agent.destination) <= 0.1f)
        {
            if (WayPointCounter >= WayPoints.Length)
            {
                WayPointCounter = 0;
            }
            agent.SetDestination(WayPoints[WayPointCounter++].transform.position);
        }
    }

    public override void resetPos()
    {
        base.resetPos();
        agent.SetDestination(WayPoints[WayPointCounter].transform.position);

    }
}
