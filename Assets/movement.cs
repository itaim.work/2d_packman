using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class movement : MonoBehaviour
{
    public float speed, StarCounter, LifeCounter, starAmount;
    public Transform movePoint;
    public GameObject[] Ghosts;
    public LayerMask whatStopsMovement;

    // Start is called before the first frame update
    void Start()
    {
        starAmount = GameObject.FindGameObjectsWithTag("Star").Length;
        Ghosts = GameObject.FindGameObjectsWithTag("Ghost");
        movePoint.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, movePoint.position, speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, movePoint.position) <= .005f)
        {
            if (!Physics2D.OverlapCircle(movePoint.position + new Vector3(0f, Input.GetAxis("Vertical"), 0f), .05f, whatStopsMovement))
            {
                movePoint.position += new Vector3(0f, Input.GetAxis("Vertical"), 0f);
            }


            if (!Physics2D.OverlapCircle(movePoint.position + new Vector3(Input.GetAxis("Horizontal"), 0f, 0f), .05f, whatStopsMovement))
            {
                movePoint.position += new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
            }


        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Star"))
        {
            StarCounter++;
            Destroy(collision.gameObject);
            print(StarCounter);
            if (StarCounter >= starAmount)
            {
                print("Level Won");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

        }
        else if (collision.CompareTag("Ghost"))
        {
            LifeCounter--;
            if (LifeCounter <= 0)
            {
                print("Level Lost");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            resetGhost();
        }
    }
    public void resetGhost()
    {
        for (int i = 0; i < Ghosts.Length; i++)
        {
            Ghosts[i].GetComponent<Ghost_Script>().resetPos();
        }
    }
}
